import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error

# generate n evenly spaced values from 0 radians to 2 PI radians
from sklearn.model_selection import train_test_split

n = 200
x = np.linspace(0, 2 * np.pi, n)
sine_values = np.sin(x)

# plot sine wave
plt.plot(x , sine_values)
#print(plt.show())

# add some noise to the sine wave
noise = 0.5
noisy_sine_values = sine_values + np.random.uniform(-noise, noise, n)

#plot noise data and the original data together
plt.plot(x, noisy_sine_values, color = 'r' )
#print(plt.show())
plt.plot(x, sine_values, linewidth=3)
#print(plt.show())

# Calculate the ME usinf the equation
error_values = (1/n) * sum(np.power(sine_values - noisy_sine_values, 2))
#print(error_value)

# use mean_squared function form sklearn library
#print(mean_squared_error(sine_values, noisy_sine_values))

def error(m ,x, c, t):
    N = x.size
    e=sum(((m * x + c) - t) ** 2)
    return e * 1/(2 * N)


#0.2 indicates 20% of the data is randomly sampled  as testing data
xtrain , xtest, ytrain, ytest = train_test_split(X ,Y, test_size = 0.2)


def update(m ,x, c, t, learning_rate):
    grad_m = sum(2 * ((m * x + c) - t) * x)
    grad_c = sum(2 * ((m * x + c) - t))
    m = m - grad_m * learning_rate
    c = c - grad_c * learning_rate
    return m, c


def gradient_descent(init_m, init_c, x, t, learning_rate, iterations, error_threshhold):
    m = init_m
    c = init_c
    error_values = list()
    mc_values= list()
    for i in range(iterations):
        e = error(m, x, c, t)
        if e < error_threshhold:
            print('error less than the threshold. Stopping gradient descent')
            break
        error_values.append(e)
        m, c = update(m, x, c, t, learning_rate)
        mc_values.append((m, c))

    return m, c, error_values, mc_values



init_m = 0.9
init_c = 0
learning_rate = 0.001
iterations = 250
error_threshold = 0.001


m, c, error_values, mc_values  = gradient_descent(init_m, init_c, xtrain, ytrain, learning_rate, iterations, error_threshold)