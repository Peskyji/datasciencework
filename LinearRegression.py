import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from sklearn.datasets import load_boston
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from IPython.display import HTML

#Load the dataset
boston = load_boston()

#Description of the dataset
#print(boston.DESCR)

#put the datat into pandas data frame
features = pd.DataFrame(boston.data, columns=boston.feature_names)

#print data
#print(features)

#select age and display it
#print(features['AGE'])

#select and store the target columns
target = pd.DataFrame(boston.target, columns=['target'])
#print(target)

#find max
#print(max(target['target']))
#find min
#print(min(target['target']))

#let us concatenate the features into single data frame
#axis=1 makes it concatenate column wise
df = pd.concat([features, target], axis=1)
#print(df)

#user round of round(decimal=2) to get precesion upto 2 decimal places
#print(df.describe().round(decimals=2))


#calculate the corellation between every column on the data
corr = df.corr('pearson')

#take absolute values of the correlation
corrs = [abs(corr[attr]['target']) for  attr in list(features)]

# make a list of pairs [[corr, feature]]
l=list(zip(corrs, list(features)))

#sort the pairs in revers/descending order,
#with the correlation value as the key sorting
l.sort(key = lambda x : x[0], reverse = True)

# "unzip" pairs to the list
#zip(*l)  takes a list that looks like [a,b,c], [d,e,f], [g,h,i]
# and returns [a,d,g], [b,e,h], [c,f,i]
corrs, labels = list(zip(*l))

#plot correlation with respect to the target variables as a bar graph
index = np.arange(len(labels))
plt.figure(figsize=(15, 5))
plt.bar(index, corrs, width=0.5)
plt.xlabel("Attributes")
plt.ylabel("Correlation with target variable")
plt.xticks(index, labels)
#plt.show()


X = df['LSTAT'].values
Y = df['target'].values

#print some values before normalization to see the deifference
#print(Y[:5])

x_scaler = MinMaxScaler()
X = x_scaler.fit_transform(X.reshape(-1, 1))
X = X[:, -1]
y_scaler = MinMaxScaler()
Y = y_scaler.fit_transform(Y.reshape(-1, 1))
Y = Y[:, -1]

#print(Y[:5])


n = 200
x = np.linspace(0, 2 * np.pi, n)
sine_values = np.sin(x)

# plot sine wave
plt.plot(x , sine_values)
#print(plt.show())

# add some noise to the sine wave
noise = 0.5
noisy_sine_values = sine_values + np.random.uniform(-noise, noise, n)

#plot noise data and the original data together
plt.plot(x, noisy_sine_values, color = 'r' )
#print(plt.show())
plt.plot(x, sine_values, linewidth=3)
#print(plt.show())

# Calculate the ME usinf the equation
error_values = (1/n) * sum(np.power(sine_values - noisy_sine_values, 2))
#print(error_value)

# use mean_squared function form sklearn library
#print(mean_squared_error(sine_values, noisy_sine_values))

def error(m ,x, c, t):
    N = x.size
    e=sum(((m * x + c) - t) ** 2)
    return e * 1/(2 * N)


#0.2 indicates 20% of the data is randomly sampled  as testing data
xtrain , xtest, ytrain, ytest = train_test_split(X ,Y, test_size = 0.2)


def update(m ,x, c, t, learning_rate):
    grad_m = sum(2 * ((m * x + c) - t) * x)
    grad_c = sum(2 * ((m * x + c) - t))
    m = m - grad_m * learning_rate
    c = c - grad_c * learning_rate
    return m, c


def gradient_descent(init_m, init_c, x, t, learning_rate, iterations, error_threshhold):
    m = init_m
    c = init_c
    error_values = list()
    mc_values= list()
    for i in range(iterations):
        e = error(m, x, c, t)
        if e < error_threshhold:
            print('error less than the threshold. Stopping gradient descent')
            break
        error_values.append(e)
        m, c = update(m, x, c, t, learning_rate)
        mc_values.append((m, c))

    return m, c, error_values, mc_values



init_m = 0.9
init_c = 0
learning_rate = 0.001
iterations = 250
error_threshold = 0.001


m, c, error_values, mc_values  = gradient_descent(init_m, init_c, xtrain, ytrain, learning_rate, iterations, error_threshold)


# visualization part using the values generate a video of the inputs
# as the number of iterations increases, changes in the line are less noticiable
# inorder to reduce the processing timefor the animations, it is advised to choose smaller interval
mc_values_anim = mc_values[0:250:5]
fig, ax = plt.subplots()
ln, = plt.plot([], [], 'ro-', animated = True)

def init():
    plt.scatter(xtest, ytest, color = 'g')
    ax.set_xlim(0, 1.0)
    ax.set_ylim(0, 1.0)
    return ln,

def update_frame(frame):
    m, c = mc_values_anim[frame]
    x1, y1 = -0.5, m * -.5 + c
    x2, y2 = -1.5, m * -1.5 + c
    ln.set_data([x1, x2], [y1, y2])
    return ln,

anim = FuncAnimation(fig, update_frame, frames=range(len(mc_values_anim)), init_func = init, blit=True)
#HTML(anim.to_html5_video())
#HTML(anim.to_html5_video())


plt.scatter(xtrain, ytrain, color='b')
plt.plot(xtrain, (m * xtrain + c), color='r')
#plt.show()
plt.plot(np.arange(len(error_values)), error_values)
plt.ylabel('Error')
plt.xlabel('Iterations')
plt.show()